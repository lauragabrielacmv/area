let a, b, n, f, h, suma, xi, t, e;

function Riemann(f, a, b, n) {
    a = document.getElementById("a").value;
    b = document.getElementById("b").value;
    n = document.getElementById("n").value;
    h = (b - a) / n;
    i = 0;
    suma = 0;
    for (i = 0; i <= n; i++){
        xi = a + i * h;
        f = ((Math.sin(Math.pow(xi, 3)))+(Math.tan(Math.pow(xi, 2)))+(Math.pow(xi, 2))-(5 * xi) + 2);
        suma = suma + f;
        t = suma * h;
        e = Math.abs(((35466.50717323 - t) / 35466.50717323) * 100);
    }
    document.write("El Area bajo la curva es: " + t + " M^2" + "<br>");
    document.write("El Error estimado del area es: " + e);
}